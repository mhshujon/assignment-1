<?php


class ValidationMethods
{
    //UIU CSE ID Variables Starts
    private $stringLenth = 9;
    private $cseCode = 11;
    private $startingYear = 03;
    private $recentYear = 19;
    private $startingSemester = 1;
    private $endSemester = 3;
    private $studentIDStarts = 001;
    private $studentIDLasts = 999;
    //UIU CSE ID Variables Ends

    //Complex Variables Starts
    private $imaginaryNumber = 'i';
    private $minLength = 4;
    private $maxLength = 6;
    //Complex Variables Ends

    public function matchID($id){
        $id = $id;

        if (strlen($id) == $this->stringLenth){
            //splitting the given ID into an array
            $arrID = str_split($id);

            //storing the 4 parts of the ID into 4 separate arrays
            for ($i=0; $i<$this->stringLenth; $i++){
                if ($i >= 0 && $i <= 2){
                    $firstThree[$i]= $arrID[$i];
                }
                elseif ($i >= 3 && $i <= 4){
                    $middleFirstTwo[$i]= $arrID[$i];
                }
                elseif ($i == 5){
                    $middleLastOne= $arrID[$i];
                }
                elseif ($i >= 6 && $i <= 8){
                    $lastThree[$i]= $arrID[$i];
                }
            }
            //converting arrayToString
            $first = implode($firstThree);
            $middleTwo = implode($middleFirstTwo);
            $last = implode($lastThree);

            //comparing the strings with default values
            if ($first == $this->cseCode){
                if ($middleTwo >= $this->startingYear && $middleTwo <= $this->recentYear){
                    if ($middleLastOne >= $this->startingSemester && $middleLastOne <= $this->endSemester){
                        if ($last >= $this->studentIDStarts && $last <= $this->studentIDLasts){
                            return 1;
                        }
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
                else
                    return 0;
            }
            else
                return 0;
        }
        else
            return 0;
    }

    public function complexVariables($compVar){
        $compVar = $compVar;

        // format: a+ib
        if (strlen($compVar) == $this->minLength){
            //splitting the given Complex Variable into an array
            $arrCompVar = str_split($compVar);

            for ($i=0; $i<$this->minLength;){
                if ((($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') || ($arrCompVar[$i] >= '1')) && ($arrCompVar[$i] != $this->imaginaryNumber)){ // i=0
                    $i++;
                    if ($arrCompVar[$i] == '+'){ // i=1
                        $i++;
                        if (((($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') || ($arrCompVar[$i] >= '1')) && ($arrCompVar[$i-2] != $arrCompVar[$i])) || ($arrCompVar[$i] == $this->imaginaryNumber)){ // i=2
                            $i++;
                            if ($arrCompVar[$i-1] != $this->imaginaryNumber){ // i=3
                                if ($arrCompVar[$i] == $this->imaginaryNumber){ // i=3
                                    return 1;
                                }
                                else
                                    return 0;
                            }
                            elseif ($arrCompVar[$i-1] == $this->imaginaryNumber){ // i=3
                                if ((($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') || ($arrCompVar[$i] >= '1')) && ($arrCompVar[$i-3] != $arrCompVar[$i]) && ($arrCompVar[$i-1] != $arrCompVar[$i])){
                                    return 1;
                                }
                                else
                                    return 0;
                            }
                            else
                                return 0;
                        }
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
                else
                    return 0;
            }
        }

        // format: z=a+ib
        elseif (strlen($compVar) == $this->maxLength){
            //splitting the given Complex Variable into an array
            $arrCompVar = str_split($compVar);
            for ($i=0; $i<$this->minLength;){
                if (($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') && ($arrCompVar[$i] != $this->imaginaryNumber)){ // i=0
                    $i++;
                    if ($arrCompVar[$i] == '='){ // i=1
                        $i++;
                        if ((($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') || ($arrCompVar[$i] >= '1')) && ($arrCompVar[$i] != $this->imaginaryNumber) && ($arrCompVar[$i] != $arrCompVar[$i-2])){ // i=2
                            $i++;
                            if ($arrCompVar[$i] == '+'){ // i=3
                                $i++;
                                if (((($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') || ($arrCompVar[$i] >= '1')) && ($arrCompVar[$i-2] != $arrCompVar[$i]) && ($arrCompVar[$i] != $arrCompVar[$i-4])) || ($arrCompVar[$i] == $this->imaginaryNumber)){ // i=4
                                    $i++;
                                    if ($arrCompVar[$i-1] != $this->imaginaryNumber){ // i=5
                                        if ($arrCompVar[$i] == $this->imaginaryNumber){ // i=5
                                            return 1;
                                            echo '1';
                                        }
                                        else
                                            return 0;
                                            echo '2';
                                    }
                                    elseif ($arrCompVar[$i-1] == $this->imaginaryNumber){ // i=5
                                        if ((($arrCompVar[$i] >= 'a' && $arrCompVar[$i] <= 'z') || ($arrCompVar[$i] >= '1')) && ($arrCompVar[$i-4] != $arrCompVar[$i]) && ($arrCompVar[$i-1] != $arrCompVar[$i])  && ($arrCompVar[$i-5] != $arrCompVar[$i])){
                                            return 1;
                                            echo '3';
                                        }
                                        else
                                            return 0;
                                            echo '4';
                                    }
                                    else
                                        return 0;
                                        echo '5';
                                }
                                else
                                    return 0;
                                    echo '6';
                            }
                            else
                                return 0;
                                echo '7';
                        }
                        else
                            return 0;
                            echo '8';
                    }
                    else
                        return 0;
                        echo '9';
                }
                else
                    return 0;
                    echo '10';
            }
        }
        else
            return 0;
            echo '11';
    }

    public function quesThree($usrInpt){
        $userInput = $usrInpt;

        if (strlen($userInput) >= 3){
            //splitting the given ID into an array
            $arrID = str_split($userInput);

            for ($i=0; $i<3;){
                if ($arrID[$i] >= 0 || $arrID[$i] <= 0){
                    $i++;
                    if ($arrID[$i] >= 'A' && $arrID[$i] <= 'Z'){
                        $i++;
                        if ($arrID[$i] >= 'A' && $arrID[$i] <= 'Z' || $arrID[$i] >= 'a' && $arrID[$i] <= 'z'){
                            return 0;
                        }
                        else
                            return 1;
                    }
                    else
                        return 0;
                }
                else
                    return 0;
            }
        }
        else
            return 0;
    }

    public function integerFloat($intgrFlt){
        $integerFloat = $intgrFlt;

        $arrID = str_split($integerFloat);
           $count = 0;
        if ($arrID[strlen($integerFloat)-1] != '.'){
            for ($i=0; $i<strlen($integerFloat); $i++){
                        $count++;
                        if (($arrID[$i]) >= 'A' && $arrID[$i] <= 'Z') || ($arrID[$i]) >= 'a' && $arrID[$i] <= 'z')){
                            return 0;
                        }
                        elseif ((intval($arrID[$i]) >= 0 && intval($arrID[$i]) <= 0) || ($arrID[$i] == '+' || $arrID[$i] == '-' || $arrID[$i] == '.')){
                            if ($arrID[$i] == '.'){
                                $i++;
                                if (($arrID[$i]) >= 'A' && $arrID[$i] <= 'Z') || ($arrID[$i]) >= 'a' && $arrID[$i] <= 'z') || ($arrID[$i] == '.') || ($arrID[$i] == '+' || $arrID[$i] == '-') || ($arrID[$i] == '') || ($arrID[$i] == NULL)){
                                    return 0;
                                }
                                else
                                    return 1;
                            }
                        }
                        else{
                            if ($count == strlen($integerFloat)){
                                return 1;
                            }
                        }
                    }
        }
    }

    public function password($password){
        $password = $password;
        $count = 0;
        if (strlen($password) >= 8){
            $arrID = str_split($password);
            if ($arrID[strlen($password)-1] == '@' || $arrID[strlen($password)-1] == '#' || $arrID[strlen($password)-1] == '$' || $arrID[strlen($password)-1] == '&'){
                for ($i=0; $i<strlen($password)-1; $i++){
                    $count++;
                    if ($arrID[$i] >= 'A' && $arrID[$i] <= 'Z'){
                        $count=0;
                        for ($j=0; $j<strlen($password)-1; $j++){
                            $count++;
                            if ($arrID[$j] >= 'a' && $arrID[$j] <= 'z'){
                                $count=0;
                                for ($k=0; $k<strlen($password)-1; $k++){
                                    $count++;
                                    if ($arrID[$k] >= 0){
//                                        echo '1';
                                        return 1;
                                    }
                                    else{
                                        if ($count == strlen($password)-1)
//                                            echo '2';
                                            return 0;
                                    }
                                }
                            }
                            else{
                                if ($count == strlen($password)-1)
//                                    echo '3';
                                    return 0;
                            }
                        }
                    }
                    else{
                        if ($count == strlen($password)-1)
//                            echo '4';
                            return 0;
                    }
                }
            }
            else
//                echo '5';
                return 0;
        }
        else
//            echo '6';
            return 0;
    }
}










































