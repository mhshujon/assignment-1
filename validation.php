<?php
include_once ('ValidationMethods.php');
session_start();

$object = new ValidationMethods();
if (isset($_POST['id'])){
    if ($_POST['id'] != NULL){
        $id = $_POST['id'];
        $result = $object->matchID($id);
    }
}
if (isset($_POST['compVar'])){
    if ($_POST['compVar'] != NULL){
        $compVar = $_POST['compVar'];
        $result = $object->complexVariables($compVar);
    }
}
if (isset($_POST['usrInpt'])){
    if ($_POST['usrInpt'] != NULL){
        $usrInpt = $_POST['usrInpt'];
        $result = $object->quesThree($usrInpt);
    }
}
if (isset($_POST['intgrFlt'])){
    if ($_POST['intgrFlt'] != NULL){
        $intgrFlt = $_POST['intgrFlt'];
        $result = $object->integerFloat($intgrFlt);
    }
}
if (isset($_POST['password'])){
    if ($_POST['password'] != NULL){
        $password = $_POST['password'];
        $result = $object->password($password);
    }
}
if ($result == 1){
    echo 'Valid ID';
    $_SESSION['validity'] = 'yes';
    header('location: index.php');
}
else{
    echo 'Invalid ID';
    $_SESSION['validity'] = 'no';
    header('location: index.php');
}
