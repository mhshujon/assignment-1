<?php
session_start();

if (isset($_SESSION['validity'])){
    if ($_SESSION['validity'] == 'yes'){
        echo "<script>window.alert('Valid Input')</script>";
        session_destroy();
    }
    elseif ($_SESSION['validity'] == 'no'){
        echo "<script>window.alert('Invalid Input')</script>";
        session_destroy();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Compiler Assignments</title>

    <!-- Font Awesome -->
<!--    <link rel="stylesheet" href="assets/font/all.css">-->
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <h2 class="input-1">1. Design a regular expression that would accept only UIU CSE ID.</h2>
    <form action="validation.php" method="POST">
        <input class="form-control form-control-lg input-1" name="id" type="text" placeholder="Your Department ID (CSE)">
        <button type="submit" class="btn btn-primary btn-rounded btn-1">Submit</button>
    </form>

    <h2 class="input-1">2. Design a regular expression that would accept only general complex number format.</h2>
    <form action="validation.php" method="POST">
        <input class="form-control form-control-lg input-1" name="compVar" type="text" placeholder="Complex Variable">
        <button type="submit" class="btn btn-primary btn-rounded btn-1">Submit</button>
    </form>

    <h2 class="input-1">3. Design a regular expression that would accept a string with the following criteria</h2>
    <ul>
        <li>Start with a digit</li>
        <li>Second character is any capital English alphabet</li>
        <li>Third character cannot be alphabet</li>
        <li>Fourth to rest, combination of both alphabet and character</li>
    </ul>
    <form action="validation.php" method="POST">
        <input class="form-control form-control-lg input-1" name="usrInpt" type="text" placeholder="Your input">
        <button type="submit" class="btn btn-primary btn-rounded btn-1">Submit</button>
    </form>

    <h2 class="input-1">4. Design a regular expression that would accept both integer and floating point number.</h2>
    <form action="validation.php" method="POST">
        <input class="form-control form-control-lg input-1" name="intgrFlt" type="text" placeholder="Integer/Float Number">
        <button type="submit" class="btn btn-primary btn-rounded btn-1">Submit</button>
    </form>

    <h2 class="input-1">5. Design the regular expression that would accept password based on the following criteria.</h2>
    <ul>
        <li>At least eight characters.</li>
        <li>At least one uppercase character.</li>
        <li>At least one lowercase character.</li>
        <li>At least one number.</li>
        <li>Finish with a special character ( @, #, $, &)</li>
    </ul>
    <form action="validation.php" method="POST">
        <input class="form-control form-control-lg input-1" name="password" type="text" placeholder="Give a password">
        <button type="submit" class="btn btn-primary btn-rounded btn-1">Submit</button>
    </form>
</div>

<!-- JQuery -->
<script type="text/javascript" src="assets/js/jquery-3.4.0.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="assets/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="assets/js/mdb.min.js"></script>
</body>
</html>